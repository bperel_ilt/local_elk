## DEPLOY A LOGGING INFRA : ELK STACK v5

![elkv5.png](https://github.com/gregbkr/elk-dashboard-v5-docker/raw/master/elkv5.PNG)

This setup will run elk
* elasticsearch v5 database
* Logstash v5: receiving and parsing logs
* Kibana v5: Web interface
* Docker container is the main source of logs, but we could send anything to syslog 5000/udp

Prerequisite:
 - Linux like OS
 - Docker, docker-compose

More info: you can find an overview of that setup on my blog: https://greg.satoshi.tech/

## 1. Get all files from github
    git clone git@bitbucket.org:bperel_ilt/local_elk.git elk && cd elk
    
## 2. Make all containers redirect their logs to ELK

    sudo cp daemon.json /etc/docker && sudo service docker restart

## 3. Fix
Fix an issue with hungry es v5
    
    sudo sysctl -w vm.max_map_count=262144

make it persistent: 

    nano /etc/sysctl.conf
    vm.max_map_count=262144

## 4. Run all containers for version 5:
    
    docker-compose up -d

and send few logs with nc or socat:

    nc -w0 -u localhost 5000 <<< "TEST1"
    echo "`date +\%Y-\%m-\%dT\%H:\%M:\%S` vm:`hostname` service:.com.health msg:TEST2" | socat -t 0 - UDP:localhost:5000

## 5. Log on kibana to see the result

[http://localhost:5601](http://localhost:5601)

Initialize the index: pressing the green "create" button when log starting to come.

## 6. Debug

If something is going wrong, try uncommenting the 3 lines at the beginning of `logstash.conf` and then :
    
    docker-compose restart logstash && docker-compose logs -f logstash
    
It should dump the logs that Logstash receives, it can help understand what is going wrong with their processing.



